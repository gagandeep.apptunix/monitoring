const config = require('config')
module.exports = {
    CODES: require('./codes'),
    MESSAGES: require('./messages'),
    LANGS: require('./langs'),
    REGEX: {
        EMAIL: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
        PHONE: /^[0-9]+$/,
        COUNTRY_CODE: /^[0-9,+]+$/,
        PASSWORD: /^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[a-zA-Z!#$%&? " + "])[a - zA - Z0 - 9!#$ %&?]{ 8, 20 } $ /
    },
    ENUMS: {
        DEVICES: [
            0, // ANDROID
            1, // IOS
            2  // WEB
        ]
    },
    OTP_OPTIONS: {
        LENGTH: 5,
        EXPIRES: 1,
        IN: "minutes"
    },
    EMAIL_SERVICE: {
        USING: config.get('EMAIL_SERVICE'), // 0:NODE_MAILER   1:SENDGRID
        NODE_MAILER: {
            EMAIL: "gagandeep.apptunix@gmail.com",
            PASSWORD: "54321@ASDFg"
        },
        SEND_GRID: {
            EMAIL: "gagandeep.apptunix@gmail.com",
        },
        SUBJECTS: {
            REMINDER: "Booking Tommorow"
        }
    },
    PATHS: {
        IMAGE: {
            ADMIN: {
                ACTUAL: "/uploads/images/admin/",
                STATIC: "/admin/profile/image/"
            }
        },
        FILE: {
            ADMIN: {
                ACTUAL: "/uploads/files/driver/",
                STATIC: "/driver/profile/document/"
            }
        }
    }
}