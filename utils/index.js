const Messages = require('../constants').LANGS
const bcrypt = require('bcryptjs')
const config = require('config')
const jwt = require('jsonwebtoken')
const fs = require("fs");
const path = require("path");
const ffmpeg = require('fluent-ffmpeg')
const allRoutes = require('express-list-endpoints')
const joiToSwagger = require('joi-to-swagger');
let swagger = require('../swagger.json')
const validations = require('../v1/validations');
const Logs = require('../models').logs
const SOCKETS = require('./Sockets').Socket
module.exports = {
    /*
    Response Functions
    */
    response: async (res, status, message, data, lang) => {
        if (status != 200) {
            let log = await Logs.findByIdAndUpdate(res.id, { code: status, message: message, res: JSON.stringify(data), resTime: getMillinseconds(res.time), error: true }, { new: true }).lean().exec()
            SOCKETS.emit('getLog', log)
            return await res.status(status).send({ status: status, message: await Messages[lang][message] });
        }
        let log = await Logs.findByIdAndUpdate(res.id, { code: status, message: message, res: JSON.stringify(data), resTime: getMillinseconds(res.time) }, { new: true }).lean().exec()
        SOCKETS.emit('getLog', log)
        return await res.status(status).send({ status: status, message: await Messages[lang][message], result: data });
    },
    getMillinseconds: (time) => {
        return (Math.abs((new Date().getTime() - time) / 1000)) * 1000;
    },
    /*
    Bcrypt Functions
    */
    hashPasswordUsingBcrypt: async (password) => { return bcrypt.hashSync(password, 10); },
    comparePasswordUsingBcrypt: async (pass, hash) => { return bcrypt.compareSync(pass, hash) },
    /*
    JWT Functions
    */
    jwtSign: async (payload) => {
        try {
            return jwt.sign(
                { _id: payload._id },
                config.get("JWT_OPTIONS").SECRET_KEY,
                {
                    expiresIn: config.get("JWT_OPTIONS").EXPIRES_IN
                }
            );
        } catch (error) {
            throw error;
        }
    },
    jwtVerify: async (token) => {
        try {
            return jwt.verify(token, config.get("JWT_OPTIONS").SECRET_KEY);
        } catch (error) {
            throw error;
        }
    },
    /*
    Generate Thumbnail Functions
    */
    generateVideoThumbnail: async (paths, saveLocation) => {
        try {
            ffmpeg(paths)
                .screenshots({
                    filename: paths.split('/')[paths.split('/').length - 1] + "_thumbnail.png",
                    folder: path.join(__dirname, saveLocation),
                    count: 1
                }).on('error', (e) => {
                    console.log({ e })
                    return false
                })
                .on('end', async () => {
                    return true
                })
        } catch (error) {
            throw error;
        }
    },
    /*
    File Functions
    */
    deleteFiles: async (paths) => {
        await paths.forEach(filePath => fs.unlinkSync(path.resolve(__dirname, '..' + filePath)))
        return
    },
    /*
    Email Service
    */
    emailService: require('./Email'),
    /*
    Otp
    */
    generateOtp: async () => {
        try {
            var digits = '0123456789';
            let OTP = '';
            for (let i = 0; i < config.get("OTP_OPTIONS.LENGTH"); i++) { OTP += digits[Math.floor(Math.random() * 10)]; }
            return OTP;
        } catch (error) {
            throw error;
        }
    },
    /*
    Swagger Functions
    */
    generateSwagger: async (app) => {
        let swaggerData = {
            "tags": [
            ],
            "paths": {
            },
            "components": {
                "securitySchemes": {

                }
            }
        }
        let ROUTES = allRoutes(app)
        ROUTES = await convertJoiToSwagger(ROUTES)
        for (const route of ROUTES) {
            if (!swaggerData.tags.includes(route.tags[0])) swaggerData.tags.push(route.tags[0])
            let schema = {}
            schema[route.path] = {}
            schema[route.path][route.method] = {
                "tags": route.tags,
                "summary": route.summary,
                "description": route.description,
                "operationId": route.operationId,
                "parameters": route.parameters,
                "responses": {
                    "200": {
                        "description": "Success Response"
                    }
                },
                "security": route.security,
            }
            if (route.requestBody) schema[route.path][route.method]["requestBody"] = route.requestBody
            swaggerData.paths = { ...swaggerData.paths, ...schema }
        }
        /*
        add Authorizations
        */
        for (const tag of swaggerData.tags) {
            swaggerData.components.securitySchemes[tag + "Authorization"] = {
                "type": "apiKey",
                "name": "Authorization",
                "in": "header"
            }
        }
        swagger = { ...swagger, ...swaggerData }
        fs.writeFileSync('swagger.json', JSON.stringify(swagger), (err) => { if (err) throw err; })
        return

    },
    logger: async (req, res, next) => {
        console.log("API HIT", "\n|\nv\n|\nv");
        const LANGS = await getLanguages()
        if (!LANGS.includes(req.header('Accept-Language'))) { req.lang = 'en' }
        else { req.lang = req.header('Accept-Language') }
        let log = await new Logs({
            host: req.headers.host,
            url: req.url || req.originalUrl,
            userAgent: req.headers["user-agent"],
            method: req.method,
            body: JSON.stringify(req.body),
            query: JSON.stringify(req.query),
            params: JSON.stringify(req.params),
        }).save()
        res.id = log._id
        res.time = new Date().getTime()
        next();
    },
    monitoring: async (req, res, next) => {
        let logs = await Logs.find({}).sort({ createdAt: -1 }).limit(10).lean()
        var occurences = logs.reduce(function (acc, curr) {
            if (typeof acc[curr.url] == 'undefined') {
                acc[curr.url] = 1;
            } else {
                acc[curr.url] += 1;
            }
            return acc;
        }, {});
        res.render('monitoring', { data: logs, occurences: occurences })
    }
}

const convertJoiToSwagger = async (routes) => {
    let newRoutes = []
    for (const ind in routes) {
        for (const mind of routes[ind].methods) {
            let route = {}
            route.path = routes[ind].path
            route.method = mind.toString().toLowerCase()
            let tag = routes[ind].path.split("/")[3]
            route.tags = [tag]
            /*
            Check if Joi Schema Exists
            */
            let isSchemaExists = routes[ind].middleware.filter((middleware) => {
                if (middleware.includes("validate")) return middleware
            })
            if (isSchemaExists.length == 1) {
                route.operationId = tag + isSchemaExists[0]
                let { example, schema, querySchema, summary, description } = await validations[tag][isSchemaExists[0]]("swagger")
                route.summary = summary
                route.description = description
                if (querySchema) {
                    querySchema = joiToSwagger(querySchema).swagger
                    route.parameters = [{
                        name: "params",
                        in: "query",
                        schema: querySchema
                    }]
                }
                if (schema) {
                    schema = joiToSwagger(schema).swagger
                    route.requestBody = {
                        content: {
                            'application/json': {
                                schema: schema,
                                example: example
                            }
                        }
                    }
                }

            }
            /*
            Add Authorization to  API
            */
            let needAuthorization = routes[ind].middleware.filter((middleware) => {
                if (middleware.includes("Valid")) return middleware
            })
            if (needAuthorization.length == 1) {
                route.security = [{}]
                route.security[0][tag + "Authorization"] = []
            }
            newRoutes.push(route)
        }
    }
    return newRoutes
}

const getMillinseconds = (time) => {
    return (Math.abs((new Date().getTime() - time) / 1000)) * 1000;
}

/*
Get Languages List
*/
const getLanguages = async () => {
    let fileNames = []
    fs.readdirSync('constants/langs').forEach(file => {
        if (file != 'index.js') { file = file.replace('.js', ''); fileNames.push(file) }
    })
    return fileNames
}