const router = require('express').Router();
const Routes = require('./routes/');
router.use('/admin', Routes.admin);
router.use('/utils', Routes.utils)
module.exports = router;