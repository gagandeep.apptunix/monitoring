const { ENUMS, REGEX } = require('../../constants')
const joi = require("joi");
const validateSchema = async (req, res, next, schema, querySchema = false) => {
    try {
        if (schema) {
            let { error, value } = await schema.validate(req.body);
            if (error) throw error.details ? error.details[0].message.replace(/['"]+/g, '') : "";
            else next()
        }
        else if (querySchema) {
            let { error, value } = await querySchema.validate(req.query);
            if (error) throw error.details ? error.details[0].message.replace(/['"]+/g, '') : "";
            else next()
        }
    } catch (error) {
        next(error)
    }
};
module.exports = {
    validateSignup: async (req, res, next) => {
        let schema = joi.object().keys({
            firstName: joi.string().trim().required().description('firstName of Admin'),
            lastName: joi.string().trim().required().description('last Name of Admin'),
            userName: joi.string().trim().required().description('Unique User Name of Admin'),
            email: joi.string().regex(REGEX.EMAIL).trim().lowercase().required().description('Email of Admin'),
            phone: joi
                .string()
                .regex(REGEX.PHONE)
                .min(5)
                .required().description('Phone Number of Admin'),
            countryCode: joi
                .string()
                .regex(REGEX.COUNTRY_CODE)
                .trim()
                .min(2)
                .required().description('Country Code of Admin'),
            location: joi.object().keys({
                type: joi.string().trim().required(),
                coordinates: joi.array().items(joi.number().required()),
                name: joi.string().trim().required()
            }).required().description('Location of Admin'),
            profilePic: joi.string().trim().optional().description('Profile Image of Admin'),
            password: joi
                .string()
                .required()
                .description('Password of Admin Account'),
            deviceType: joi.number().valid(...ENUMS.DEVICES).required().description('Device Type of Admin'),
            deviceToken: joi.string().required().description('Device Token for Notifications'),
        });
        if (req == "swagger") {
            let example = {
                firstName: "Gagandeep",
                lastName: "Singh",
                userName: "gagan123",
                email: "gs593513@gmail.com",
                phone: "9999999999",
                countryCode: "+91",
                location: {
                    type: "Point",
                    coordinates: [20, 10],
                    name: "Mohali"
                },
                password: "234567890",
                deviceType: 0,
                deviceToken: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
            }
            let summary = "API to add Admin Account"
            let description = "API to add Admin Account"
            let swagger = {
                example: example,
                schema: schema,
                querySchema: typeof querySchema !== 'undefined' ? querySchema : false,
                summary: summary,
                description: description
            }
            return swagger
        }
        await validateSchema(req, res, next, schema)
    },
    validateVerifyUserName: async (req, res, next) => {
        let querySchema = joi.object().keys({
            userName: joi.string().trim().required().description('Unique User Name of Admin')
        });
        if (req == "swagger") {
            let summary = "API to verify Username"
            let description = "API to verify Username"
            let swagger = {
                example: typeof example !== 'undefined' ? example : false,
                schema: typeof schema !== 'undefined' ? schema : false,
                querySchema: typeof querySchema !== 'undefined' ? querySchema : false,
                summary: summary,
                description: description
            }
            return swagger
        }
        await validateSchema(req, res, next, typeof schema !== 'undefined' ? schema : false, typeof querySchema !== 'undefined' ? querySchema : false)
    },
}