const CONSTANT = require('../../constants')
const config = require('config')
const joi = require("joi");
const validateSchema = async (req, res, next, schema) => {
    try {
        let { error, value } = await schema.validate(req.body);
        if (error) throw error.details ? error.details[0].message.replace(/['"]+/g, '') : "";
        else next()
    } catch (error) {
        next(error)
    }
};
module.exports = {
    validateUserName: async (req, res, next) => {
        let schema = joi.object().keys({
            userName: joi.string().trim().required().description('Unique User Name of Admin')
        });
        if (req == "swagger") {
            let example = {
                userName: "gagan123"
            }
            let summary = "API to verify Username"
            let description = "API to verify Username"
            let swagger = {
                example: example,
                schema: schema,
                querySchema: typeof querySchema !== 'undefined' ? querySchema : false,
                summary: summary,
                description: description
            }
            return swagger
        }
        await validateSchema(req, res, next, schema)
    },
}