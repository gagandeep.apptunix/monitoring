const router = require("express").Router();
const validations = require('../validations');
const controllers = require('../controllers');
/*
On-Boarding
*/
router.post("/signup", validations.admin.validateSignup, controllers.admin.signup);
router.get("/verify/username", validations.admin.validateVerifyUserName, controllers.admin.verifyUserName);
module.exports = router