const router = require("express").Router();
const validations = require('../validations');
const controllers = require('../controllers');
/*
On-Boarding
*/
router.post("/username/verify", validations.utils.validateUserName, controllers.utils.verifyUserName);

module.exports = router