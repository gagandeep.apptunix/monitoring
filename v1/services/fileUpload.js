const multer = require('multer')
const path = require('path')
const { PATHS } = require('../../constants')





const AdminProfilePicStorage = multer.diskStorage({
    destination: function (req, file, cb) {
        let paths = path.resolve(__dirname, '../..' + PATHS.IMAGE.ADMIN.ACTUAL);
        cb(null, paths)
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + `.${file.originalname.split('.').pop()}`)
    }
});
const AdminProfilePicUpload = multer({
    storage: AdminProfilePicStorage, fileFilter: (req, file, cb) => {
        if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg") {
            cb(null, true);
        } else {
            cb(null, false);
            return cb(new Error('Only .png, .jpg and .jpeg format allowed!'));
        }
    }
})


module.exports = {
    AdminProfilePicUpload: AdminProfilePicUpload
};


