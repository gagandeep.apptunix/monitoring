const MODELS = require('../../models')
const { MESSAGES, CODES } = require('../../constants')
const universal = require('../../utils')
module.exports = {
    /*
    Admin On-Boarding
    */
    verifyUserName: async (req, res, next) => {
        try {
            let isExist = await MODELS.admin.findOne({ userName: req.query.userName }).select("userName").lean().exec()
            if (isExist) return await universal.response(res, CODES.BAD_REQUEST, MESSAGES.USERNAME_ALREADY_USED, {}, req.lang);
            return await universal.response(res, CODES.OK, MESSAGES.USERNAME_IS_VALID, {}, req.lang);
        } catch (error) {
            next(error);
        }
    },
    signup: async (req, res, next) => {
        try {
            let admin = await MODELS.admin.findOne({ email: req.body.email }).select("email").lean().exec()
            if (admin) return await universal.response(res, CODES.BAD_REQUEST, MESSAGES.EMAIL_ALREADY_ASSOCIATED_WITH_ANOTHER_ACCOUNT, {}, req.lang);
            admin = await MODELS.admin.findOne({ phone: req.body.phone }).select("phone").lean().exec()
            if (admin) return await universal.response(res, CODES.BAD_REQUEST, MESSAGES.PHONE_NUMBER_ALREADY_ASSOCIATED_WITH_ANOTHER_ACCOUNT, {}, req.lang);
            admin = await MODELS.admin.findOne({ userName: req.body.userName }).select("userName").lean().exec()
            if (admin) return await universal.response(res, CODES.BAD_REQUEST, MESSAGES.USERNAME_ALREADY_ASSOCIATED_WITH_ANOTHER_ACCOUNT, {}, req.lang);
            admin = await new MODELS.admin(req.body).save();
            return await universal.response(res, CODES.OK, MESSAGES.ADMIN_ADDED_SUCCESSFULLY, admin, req.lang);
        } catch (error) {
            next(error);
        }
    }


}