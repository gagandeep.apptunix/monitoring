const MODELS = require('../../models')
const { MESSAGES, CODES } = require('../../constants')
const universal = require('../../utils')
module.exports = {
    /*
    Admin On-Boarding
    */
    verifyUserName: async (req, res, next) => {
        try {
            let admin = await MODELS.admin.findOne({ email: req.body.email }).select("email").lean().exec()
            if (admin) return await universal.response(res, CODES.BAD_REQUEST, MESSAGES.EMAIL_ALREADY_ASSOCIATED_WITH_ANOTHER_ACCOUNT, {}, req.lang);
            admin = await MODELS.admin.findOne({ phone: req.body.phone }).select("phone").lean().exec()
            if (admin) return await universal.response(res, CODES.BAD_REQUEST, MESSAGES.PHONE_NUMBER_ALREADY_ASSOCIATED_WITH_ANOTHER_ACCOUNT, {}, req.lang);
            admin = await MODELS.admin.findOne({ phone: req.body.phone }).select("phone").lean().exec()
            if (admin) return await universal.response(res, CODES.BAD_REQUEST, MESSAGES.USERNAME_ALREADY_ASSOCIATED_WITH_ANOTHER_ACCOUNT, {}, req.lang);
            admin = await new MODELS.admin(req.body).save();
            return await universal.response(res, CODES.OK, MESSAGES.ADMIN_ADDED_SUCCESSFULLY, admin, req.lang);
        } catch (error) {
            next(error);
        }
    }

}