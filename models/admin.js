const { ENUMS } = require('../constants');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const AdminModel = new Schema({
    profilePic: {
        type: String,
        default: '/admin/profile/image/default.jpg'
    },
    firstName: {
        type: String,
        default: "",
        lowercase: true,
        trim: true
    },
    lastName: {
        type: String,
        default: "",
        lowercase: true,
        trim: true
    },
    userName: {
        type: String,
        default: '',
        required: true
    },
    email: {
        type: String,
        lowercase: true,
        trim: true
    },
    phone: {
        type: String,
        trim: true
    },
    countryCode: {
        type: String,
        trim: true
    },
    password: {
        type: String,
        default: ''
    },
    location: {
        type: {
            type: String
        },
        coordinates: [{ type: Number }],
        name: { type: String }
    },
    deviceType: {
        type: Number,
        enum: ENUMS.DEVICES, // 0:ANDROID  1:IOS  3:WEB
        required: true
    },
    deviceToken: {
        type: String,
        default: ''
    },
    isDeleted: {
        type: Boolean,
        default: false
    }
}, { timestamps: true });
AdminModel.index({ email: 1, userName: 1, phone: 1 })
const Admin = mongoose.model('Admin', AdminModel);
module.exports = Admin;