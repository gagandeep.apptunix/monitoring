const config = require('config');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const CompanyModel = new Schema({
    image: {
        type: String,
        default: '/company/profile/image/default.jpg'
    },
    name: {
        type: String,
        default: "",
        lowercase: true,
        trim: true
    },
    owner: {
        type: String,
        default: '',
        required: true
    },
    email: {
        type: String,
        lowercase: true,
        trim: true
    },
    phone: {
        type: String,
        trim: true
    },
    countryCode: {
        type: String,
        trim: true
    },
    password: {
        type: String,
        default: ''
    },
    location: {
        type: {
            type: String
        },
        coordinates: [{ type: Number }],
        name: { type: String }
    },
    deviceType: {
        type: Number,
        enum: config.get('ENUMS.DEVICES'), // 0:IOS  1:ANDROID  3:WEB
        required: true
    },
    deviceToken: {
        type: String,
        default: ''
    },
    isVerified: {
        type: Boolean,
        default: false
    },
    isDeleted: {
        type: Boolean,
        default: false
    }
}, { timestamps: true });
CompanyModel.index({ email: 1, userName: 1, phone: 1 })
const Companies = mongoose.model('Companies', CompanyModel);
module.exports = Companies;